#!/usr/bin/python3
import sys
import json
import os
import glob

def hist_cat(paths, dest_file="cat.json"):
	binslist = []
	histlist = []
	labels = []
	stats = []
	for p in paths:
		#print("Reading", p)
		f = open(p)
		o = json.load(f)
		binslist.append(o["x"])
		histlist.append(o["y"])
		stats.append(o["stats"])
		labels.append(os.path.basename(p))
		f.close()

	bins = list(set([el for lst in binslist for el in lst]))
	bins.sort()
	hist = []
	for h in range(len(histlist)):
		y = []
		for b in bins:
			try:
				i = binslist[h].index(b)
			except: i = -1
			y.append(histlist[h][i] if i >= 0 else 0)
			
		hist.append(y)

	jsonf = open(dest_file, "w")
	jobj = {
		"x" : bins,
		"y" : hist,
		"stats" : stats,
		"labels" : labels
	}
	jsonf.write(json.dumps(jobj))

if __name__ == "__main__":
	paths = sys.argv[1:]
	if len(paths) == 1 and "*" in paths[0]:
		paths = glob.glob(paths[0])
	hist_cat(paths)