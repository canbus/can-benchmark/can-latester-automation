#!/usr/bin/python3
import os
import shutil
import glob
import shutil
import time
import json

from utils import *

def clean_modules(install_dir, current_ver):
    mods = glob.glob(install_dir + "/lib/modules/*-dut")
    for m in mods:
        older2days = os.path.getmtime(m) + 2*24*60*60 < time.time()
        if os.path.basename(m) != current_ver and older2days:
            print("Removing old modules " + m)
            shutil.rmtree(m)

def overwrite_symlink(target, symlink):
    if os.path.exists(symlink):
        os.remove(symlink)
    os.symlink(target, symlink)
    return symlink

def build_and_install(out_dir: str, install_dir: str, ver = None, local_version = "-dut", quiet_make = False):
    build_dir = os.getcwd()
    print(f"Building in directory {build_dir}")
    run_cmd_safe("make olddefconfig")
    run_cmd_safe(f"make LOCALVERSION={local_version} -j4", discard_output=quiet_make)
    version_string = read_file("include/config/kernel.release")
    print("-> Built version " + version_string)

    print("-> Installing modules")
    clean_modules(install_dir, version_string)
    run_cmd_safe(f"make LOCALVERSION=-dut INSTALL_MOD_PATH={install_dir} modules_install", discard_output=True)

    if ver is None:
        ver = version_string
    
    print("-> Copying files as " + ver)
    print("Created " + shutil.copy("./arch/arm/boot/zImage", f"{out_dir}/zImage-{ver}"))
    #print("Created " + shutil.copy("./arch/arm/boot/dts/zynq-microzed-uart0.dtb", f"{out_dir}/zynq-microzed-uart0-{ver}.dtb"))
    print("Symlink " + overwrite_symlink(f"{out_dir}/zImage-{ver}", out_dir+"/zImage"))
    #print("Symlink " + overwrite_symlink(f"{out_dir}/zynq-microzed-uart0-{ver}.dtb", out_dir+"/system.dtb"))

def build_dtbocfg(source_dir: str, kernel_dir: str, install_dir: str):
    print("-> Building and installing dtbocfg")
    prev_dir = os.getcwd()
    os.chdir(source_dir)
    shutil.rmtree(os.path.join(source_dir,'build'))
    os.mkdir(os.path.join(source_dir,'build'))
    os.symlink(os.path.join('..','dtbocfg.c'), os.path.join(source_dir,'build','dtbocfg.c'))
    run_cmd_safe(f"make BUILD_DIR=./build KERNEL_SRC={kernel_dir} ARCH=arm", discard_output=False)
    run_cmd_safe(f"make BUILD_DIR=./build KERNEL_SRC={kernel_dir} ARCH=arm INSTALL_MOD_PATH={install_dir} modules_install", discard_output=True)
    os.chdir(prev_dir)

if __name__ == "__main__":
    with open("/var/lib/latester/conf.json") as conffile:
        conf = json.load(conffile)
    # Default root dir to install modules
    INSTALL_DIR=conf["INSTALL_DIR"]
    # Directory where to copy and archive built Linux images and DTBs
    OUT_DIR=conf["BUILD_OUT_DIR"]
    
    # Build in the current directory
    build_and_install(OUT_DIR, INSTALL_DIR, os.environ.get("VER"), "")
    build_dtbocfg(conf["DTBOCFG_DIR"], kernel_dir=os.getcwd(), install_dir=INSTALL_DIR)
