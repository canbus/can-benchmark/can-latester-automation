#!/usr/bin/python3
import os
import sys
import socket
import subprocess
import time
import json
import shutil
import argparse

from utils import *
from build_linux import *
from run_test import *
from process_json_dir import *

parser = argparse.ArgumentParser()
parser.add_argument("-c", "--conf", type=str, help="path  to config file")
parser.add_argument("-s", "--skip-build", action="store_true", help="run the tests on current kernel, don't build and reboot")
parser.add_argument("-q", "--quiet-make", action="store_true", help="silence output from make to log file small")
parser.add_argument("-b", "--branches", type=str, nargs="*", help="kernel variants (branches) to test, all if not specified")
args = parser.parse_args()
confname = args.conf or "/var/lib/latester/conf.json"
with open(confname) as conffile:
    conf = json.load(conffile)

TEST_RT = "rt"
TEST_BRANCHES = list(conf["KERNEL_VARIANTS"].keys())

IP_TS = conf["IP_TS"]
IP_GW = conf["IP_GW"]

MAINLINE_DIR = conf.get("MAINLINE_DIR")
INSTALL_DIR = conf["INSTALL_DIR"] # Location of (ch)root filesystem, where to install modules
BUILD_OUT_DIR = conf["BUILD_OUT_DIR"]
DTBOCFG_DIR = conf["DTBOCFG_DIR"]
REBOOT_CMD = conf["REBOOT_CMD"] #Serial reboot: 'ssh root@tborpi "ttysendbreak /dev/ttyUSB0 2500"'

RESULTS_DIR = conf["DEST_PATH"]
WEB_DIR = conf["WEB_DIR"]
OVERVIEW_CONFIGS = conf["OVERVIEW_CONFIGS"] # Which test configurations to include in the overview (eg. "master-oaat-prio")
MAKEFILE = "/var/lib/latester/GNUmakefile"

def wait_for_bootup(ip: str):
    time.sleep(10)
    s = socket.socket()
    s.settimeout(1.0)
    n = 1
    success = False
    while not success and n < 120:
        print(f"\rWaiting for bootup - {n}", end="")
        try:
            s.connect((ip, 22))
            success = True
            s.close()
        except:
            n += 1
            time.sleep(1)
    print()
    if not success:
        print("DUT did not boot within limit")
        exit(1)

def switch_kernel(branch_cfg:dict, quiet_make=False):
    # Checkout
    run_cmd_safe(["git", "-C", branch_cfg["SOURCE_DIR"], "reset", "--hard", branch_cfg["GIT_BRANCH"]], shell=False)
    
    build_dir = branch_cfg["BUILD_DIR"]
    # Prepare clean build dir
    if branch_cfg.get("CLEAN_BUILD", False):
        print("-> Setting up build dir")
        shutil.rmtree(build_dir)
        os.mkdir(build_dir)
        os.chdir(build_dir)
        shutil.copy(branch_cfg["CONFIG_FILE"], ".config")
        shutil.copy(MAKEFILE, "./")
    else:
        os.chdir(build_dir)

    # Build and install
    build_and_install(BUILD_OUT_DIR, INSTALL_DIR, ver="autobuild", quiet_make=quiet_make)
    built_ver = read_file("include/config/kernel.release")
    print(f"Built {built_ver}")
    build_dtbocfg(DTBOCFG_DIR, build_dir, INSTALL_DIR)
    print("-> Creating uboot image")
    run_cmd_safe("mkimage -f /srv/tftp/zynq/mzapo/uboot-image.its /srv/tftp/zynq/mzapo/image-dut.ub")

    # Reboot & wait
    print("-> Rebooting DUT")
    run_cmd_safe(REBOOT_CMD)
    wait_for_bootup(IP_GW)

def run_combinations(opts: list[str], kernel_ver, branch, conf):
    for i in range(2**len(opts)):
        combination = []
        for j in range(len(opts)):
            if i & (1 << j):
                combination.append(opts[j])
        run_test(combination, kernel_ver, branch, conf)

def run_tests(test_branch: str, conf, quiet_make=False):
    branch_cfg = conf["KERNEL_VARIANTS"][test_branch]
    if args.skip_build:
        os.chdir(branch_cfg["BUILD_DIR"])
    else:
        run_cmd(["git", "-C", branch_cfg["SOURCE_DIR"], "fetch", "--all"], shell=False)
        switch_kernel(branch_cfg, quiet_make)
    current_rev = subprocess.getoutput(f"git -C {branch_cfg['SOURCE_DIR']} rev-parse --short --verify HEAD")
    kernel_ver = f"{subprocess.getoutput('make LOCALVERSION= kernelrelease').splitlines()[-1]}-g{current_rev}"
    print(f"Kernel version identifier: {kernel_ver}")
    
    # Set scaling governor to performance to avoid results affected by throttling
    print("Setting perfomance scaling governor")
    run_cmd_safe(f'ssh root@{IP_GW} "echo performance | tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor"')
    
    opts = ["flood", "cangw", "stress", "fd", "thrd"]
    if test_branch == TEST_RT:
        opts.append("rt")
    run_combinations(opts, kernel_ver, test_branch, conf)
    
    # Return normal scaling governor to save power
    run_cmd_safe(f'ssh root@{IP_GW} "echo ondemand | tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor"')

print(f"-> Starting run_daily_tests.py at {time.ctime()}")

# Fetch repositories
print("-> Fetching Linux repositories...")
os.chdir(MAINLINE_DIR)
run_cmd("git fetch --all")

# Run tests
branches = args.branches or TEST_BRANCHES
for branch in branches:
    if branch not in TEST_BRANCHES:
        print(f"Invalid branch name. Specify names from {TEST_BRANCHES}")
        continue
    print("-> Testing " + branch)
    run_tests(branch, conf, args.quiet_make)
    print("-----------------")

# Process results
web_results = os.path.join(WEB_DIR, "results")
for branch in TEST_BRANCHES:
    process_json_dir(os.path.join(RESULTS_DIR, branch), web_results)
make_overview(web_results, OVERVIEW_CONFIGS, os.path.join(web_results, "overview.json"))
generate_manifest(web_results)
