#!/usr/bin/python3
import os
import sys
from datetime import datetime
import glob
import json

from hist_cat import *

def process_json_dir(path: str|list[str], dest_dir="", only_year=None):
    if isinstance(path, str):
        path = [path]
    files = []
    for p in path:
        if not os.path.exists(p):
            print(f"Path {p} does not exist")
            continue

        p = p.rstrip("/")
        dirn = os.path.basename(p)
        os.chdir(p)
        if only_year is None:
            nfiles = os.listdir(".")
        else:
            nfiles = glob.glob(f"run-{only_year-2000:02}*")
        files += [f"{p}/{f}" for f in nfiles]
    groups = {}

    for f in files:
        start = f.rfind("+") + 1
        if start < 1: continue
        end = f.rfind(".txt.")
        if end < 0:
            end = f.rfind(".json")
            if end < 0: continue
        runtype = f[start:end]
        if runtype in groups.keys():
            groups[runtype].append(f)
        else:
            groups[runtype] = [f]

    for g in groups.keys():
        target_f = os.path.join(dest_dir, f"{dirn}-{g}.json")
        print("Generating " + target_f)
        files = list(groups[g])
        files.sort()
        hist_cat(files, target_f)

def make_overview(results_dir: str, included_series: list[str], dest_file="overview.json"):
    xs = []
    ys = []
    labels = []
    last_hists = []
    for s in included_series:
        print("Generating overview for " + s)
        path = os.path.join(results_dir, s+".json")
        with open(path) as f:
            j = json.load(f)
            x = []
            for l in j["labels"]:
                split = l.split("-")
                date = split[1]
                time = split[2]
                ts = datetime.strptime(date+time, "%y%m%d%H%M%S")
                x.append(int(ts.timestamp()))
            xs.append(x)
            ys.append([stat["max"] for stat in j["stats"]])
            labels.append(s)
            last_x = j["x"]
            last_y = j["y"][-1]
            # Remove trailing zeros
            for i in range(len(last_x)-1,0,-1):
                if last_y[i] != 0:
                    last_x = last_x[0:i+2]
                    last_y = last_y[0:i+2]
                    break
            last_hist = {
                "x": last_x,
                "y": last_y,
                "label": j["labels"][-1]
            }
            last_hists.append(last_hist)
    
    out = {
        "xs": xs,
        "ys": ys,
        "labels": labels,
        "last_hists": last_hists
    }
    with open(dest_file, "w") as f:
        json.dump(out, f)

def generate_manifest(results_dir):
    print("Generating manifest.json")
    kernelOpts = {}
    for f in sorted(os.listdir(results_dir)):
        if not f.endswith(".json") or f in ["overview.json", "manifest.json"]:
            continue
        kernel, *config = f.split(".")[0].split("-")
        opts:list = kernelOpts.get(kernel, [])
        for c in config:
            if c != "oaat" and c not in opts:
                opts.append(c)
        kernelOpts[kernel] = opts
    manifest = {"kernelOpts" : kernelOpts}
    with open(os.path.join(results_dir, "manifest.json"), "w") as f:
        json.dump(manifest, f, indent=2)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Too few arguments")
        exit(1)

    process_json_dir(sys.argv[1])
