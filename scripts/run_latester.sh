#!/bin/bash

: ${IP_TS:=10.42.0.50}
: ${OPTS:="-o -c 10000"}
echo Running test on "$IP_GW"

ssh root@"$IP_TS" "can-latester/_compiled/bin/latester -d can2 -d can3 -d can4 -n auto $OPTS"
scp root@"$IP_TS":auto-* .

#draw-hist.py auto-hist.txt hist.png || exit $?

DATEFMT=$(date +%y%m%d-%H%M%S)
cp auto-hist.txt run-$DATEFMT-hist+$1.txt
cp auto-stat.txt run-$DATEFMT-stat+$1.txt
#cp hist.png run-$DATEFMT-plot-+$1.png
hist_to_json.py run-$DATEFMT-hist+$1.txt

#xdg-open hist.png

