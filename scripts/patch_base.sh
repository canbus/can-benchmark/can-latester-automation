#!/bin/bash

#Exit on error
set -e

PATCH_DIR="/var/lib/latester/patches/"
git am "$PATCH_DIR/0001-ARM-zynq-Include-DTS-variant-for-MicroZed-board-with.v2.patch"
git am "$PATCH_DIR/0002-ARM-zynq-MicroZed-Left-FPGA-clocks-enabled-after-boo.patch"
git am "$PATCH_DIR/timestamping/0001-can-ctucanfd-add-HW-timestamps.rebased.patch"
git am "$PATCH_DIR/timestamping/0002-dt-bindings-can-ctucanfd-add-properties-for-HW-times.patch"
git am "$PATCH_DIR/timestamping/0003-doc-ctucanfd-RX-frames-timestamping-for-platform-dev.patch"
git am "$PATCH_DIR/timestamping/0004-fix-build.patch"

