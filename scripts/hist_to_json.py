#!/usr/bin/python3
import os
import sys
import json
import math

DECIMALS = 3
STEP = 1 / (10**DECIMALS)

def hist_to_json(histp: str):
	histf = open(histp)
	histbins = []
	histcum = []
	for l in histf:
		ls = l.strip()
		if len(ls) < 1: continue
		la = ls.split()
		histbins.append(float(la[0]))
		histcum.append(int(la[1]))

	hist = [0 for _ in range(len(histbins))]
	hist[-1] = histcum[-1]
	for i in range(len(histbins)-2,-1,-1):
		hist[i] = histcum[i]-histcum[i+1]

	newbins = []
	newhist = []
	for i in range(len(histbins)):
		if i > 0 and round(newbins[-1],DECIMALS) != round(histbins[i] - STEP, DECIMALS):
			newbins.append(round(histbins[i] - STEP, DECIMALS))
			newhist.append(0)
		newbins.append(histbins[i])
		val = hist[i]
		newhist.append(val)
		if i >= len(histbins)-1 or round(histbins[i+1],DECIMALS) != round(histbins[i] + STEP, DECIMALS):
			newbins.append(round(histbins[i] + STEP, DECIMALS))
			newhist.append(0)

	statp = histp.replace("-hist", "-stat")
	statf = open(statp)
	#statlist = ["percentile5", "percentile95", "lost", "out_of_range_below", "out_of_range_above"]
	stats = {}
	for line in statf:
		l = line.split("=")
		if l[0] == "avg":
			stats["average"] = float(l[1]) / 1000.
		elif l[0] == "percentile0":
			stats["min"] = int(l[1]) / 1000.
		elif l[0] == "percentile5":
			stats["perc5"] = int(l[1]) / 1000.
		elif l[0] == "percentile50":
			stats["med"] = int(l[1]) / 1000.
		elif l[0] == "percentile95":
			stats["perc95"] = int(l[1]) / 1000.
		elif l[0] == "percentile100":
			stats["max"] = int(l[1]) / 1000.
		elif l[0] == "lost":
			stats["lost"] = int(l[1])
		elif l[0] == "out_of_range_below":
			stats["out_of_range_b"] = int(l[1])
		elif l[0] == "out_of_range_above":
			stats["out_of_range_a"] = int(l[1])

	if histp.endswith(".txt"):
		jsonp = histp[:-4]+".json"
	else:
		jsonp = histp + ".json"
	jsonf = open(jsonp, "w")
	jobj = {
		"x" : newbins,
		"y" : newhist,
		"stats" : stats
	}
	jsonf.write(json.dumps(jobj))

if __name__ == "__main__":
	hist_to_json(sys.argv[1])
