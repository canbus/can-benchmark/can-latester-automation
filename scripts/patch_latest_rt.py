#!/usr/bin/python3
import os
import sys
import re

import requests
from packaging import version

RT_ROOT_URL = "https://cdn.kernel.org/pub/linux/kernel/projects/rt/"
DOWNLOAD_LOCATION = "/var/lib/latester/patches/"
if not os.path.exists(DOWNLOAD_LOCATION):
	os.makedirs(DOWNLOAD_LOCATION)

try:
	res = requests.get(RT_ROOT_URL, timeout=5)
except requests.exceptions.ConnectTimeout:
	print("Timed out waiting to get latest RT kernel version", file=sys.stderr)
	exit(1)

if res.status_code != 200:
	print(f"Got HTTP error {res.status_code}", file=sys.stderr)
	exit(1)

latest = "0.0"
latest_ver = version.parse(latest)
p = re.compile('<a href="([0-9.]*)/"')
for line in res.text.splitlines(False):
	if line.startswith("<a href="):
		ver = p.match(line).group(1)
		#print(ver)
		new_ver = version.parse(ver)
		if new_ver > latest_ver:
			#print("newer")
			latest = ver
			latest_ver = new_ver

rt_kernel_url = RT_ROOT_URL + latest + "/"

print("Latest kernel:", rt_kernel_url)
try:
	res = requests.get(rt_kernel_url, timeout=5)
except requests.exceptions.ConnectTimeout:
	print("Timed out waiting to get latest RT patch", file=sys.stderr)
	exit(1)
if res.status_code != 200:
	print(f"Got HTTP error {res.status_code}", file=sys.stderr)
	exit(1)

latest_patch = None
latest_ver = version.parse("0.0")
p = re.compile('<a href="(patch-(.*)\.gz)"')

for line in res.text.splitlines():
	m = p.match(line)
	if m is None: continue
	patch = m.group(1)
	ver = m.group(2).replace("-", ".")
	if ver.endswith(".patch"):
		ver = ver[:-6]
	ver = re.sub("[a-z]", "", ver)
	new_ver = version.parse(ver)
	if new_ver > latest_ver:
		latest_patch = patch
		latest_ver = new_ver

patch_url = rt_kernel_url+latest_patch
print("Downloading", patch_url)
download_path = DOWNLOAD_LOCATION + "/" + latest_patch
unzipped_path = DOWNLOAD_LOCATION + "/" + latest_patch[:-3]
if os.path.exists(unzipped_path):
	print("Already downloaded")
else:
	with requests.get(patch_url, stream=True) as r:
		r.raise_for_status()
		with open(download_path, 'wb') as f:
			for chunk in r.iter_content(chunk_size=8192): 
				f.write(chunk)
	print("File saved as " + download_path)
	os.system(f"gunzip -k {download_path}")

p2 = re.compile("patch-(.*)-rt")
kernel_version = p2.match(latest_patch).group(1)
ret = os.system(f"git checkout master")
if ret != 0: exit(os.WEXITSTATUS(ret))
ret = os.system(f"git pull --ff-only")
if ret != 0: exit(os.WEXITSTATUS(ret))
os.system("git rev-parse --short --verify HEAD >/var/lib/latester/current-rev")
ret = os.system(f"git checkout -B autopatch-rt v{kernel_version}")
if ret != 0: exit(os.WEXITSTATUS(ret))
ret = os.system(f"git apply {unzipped_path}")
if ret != 0: exit(os.WEXITSTATUS(ret))
os.system("git add .")
os.system(f'git commit -m "{latest_patch[:-3]}"')
ret = os.system("patch_base.sh")
if ret != 0: exit(os.WEXITSTATUS(ret))
