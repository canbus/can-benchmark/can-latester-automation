#!/usr/bin/python3
import os
import json
from pathlib import Path
import shutil

confpath = os.environ.get("LATESTER_CONF", "/var/lib/latester/conf.json")
with open(confpath) as conffile:
	conf = json.load(conffile)

web_dir = conf["WEB_DIR"]
overview_cfgs = conf["OVERVIEW_CONFIGS"]
data_range = "<p>This is an archived version of the results</p>"

def get_data_range_html():
    res_dir = conf["WEB_ARCHIVE_DIR"]
    subdirs = []
    for f in os.listdir(res_dir):
        d = os.path.join(res_dir, f)
        if os.path.isdir(d):
            subdirs.append(f)
    if len(subdirs) == 0:
        return ""
    html = "<a class=\"link\" onclick=\"updateparam('archive', '')\">Current</a>"
    for d in sorted(subdirs, reverse=True):
        html += f" <a class=\"link\" onclick=\"updateparam('archive', '{d}')\">{d}</a>"
    return "<p>Data range: " + html + "</p>"

def build_compare(web_out_dir):
    res_files = os.listdir(os.path.join(web_dir, "results"))
    test_configs = []
    test_config_items_html = ""
    for f in res_files:
        if f not in ["overview.json", "manifest.json", "retcode", "daily-log.txt"]:
            config_name = f.split(".")[0]
            test_configs.append(config_name)
    test_configs.sort()

    for c in test_configs:
        test_config_items_html += f"<div class=\"dropdown-item\" onclick=\"selectConfig('{c}')\">{c}</div>\n"

    comapare_html = Path(web_dir, "compare.html.in").read_text()
    comapare_html = comapare_html.replace("{data-range}", data_range)
    comapare_html = comapare_html.replace("{config-items}", test_config_items_html)
    Path(web_out_dir, "compare.html").write_text(comapare_html)

def build_overview(web_out_dir, archive):
    hist_boxes_html = ""
    for i in range(len(overview_cfgs)):
        hist_boxes_html += f'<div class="box halfbox">\n<h4 id="plottitle{i}">Last histogram</h4>\n<div id="plot{i}"></div>\n</div>\n'
    
    overview_html = Path(web_dir, "index.html.in").read_text()
    overview_html = overview_html.replace("{data-range}", data_range)
    overview_html = overview_html.replace("{hist-boxes}", hist_boxes_html)
    overview_html = overview_html.replace("{last-exec-status-display}", "block" if not archive else "none")
    Path(web_out_dir, "index.html").write_text(overview_html)

def build_inspect(web_out_dir):
    kernels = '"' + '", "'.join(conf["KERNEL_VARIANTS"].keys()) + '"'
    kernel_btns = ""
    for kern in conf["KERNEL_VARIANTS"].keys():
        kernel_btns += f'<button class="radiobtn" id="kern{kern}" onclick="switchkernel(\'{kern}\')">{conf["KERNEL_VARIANTS"][kern]["NAME"]}</button>'
    inspect_html = Path(web_dir, "inspect.html.in").read_text()
    inspect_html = inspect_html.replace("{kernels}", kernels)
    inspect_html = inspect_html.replace("{data-range}", data_range)
    inspect_html = inspect_html.replace("{kernel-btns}", kernel_btns)
    Path(web_out_dir, "inspect.html").write_text(inspect_html)

def build_all(web_out_dir, archive=False):
    # No data range switch when not called from main - used for archive
    build_compare(web_out_dir)
    build_overview(web_out_dir, archive)
    build_inspect(web_out_dir)
    if web_out_dir != web_dir:
        for f in os.listdir(web_dir):
            if f.endswith(".js") or f.endswith(".css"):
                shutil.copy(os.path.join(web_dir, f), os.path.join(web_out_dir, f))

if __name__ == "__main__":
    data_range = get_data_range_html()
    build_all(web_dir)
