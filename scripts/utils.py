import sys
import subprocess
from pathlib import Path

# Run cmd and exit if it fails
def run_cmd_safe(command: str, discard_output = False, shell=True):
    ret = run_cmd(command, discard_output, shell)
    if ret != 0:
        exit(ret)

def run_cmd(command: str, discard_output=False, shell=True) -> int:
    sys.stdout.flush() # Avoid reordering of output when redirected to file
    stdout = subprocess.DEVNULL if discard_output else None #None -> write to terminal
    result = subprocess.run(command, shell=shell, stdout=stdout)
    return result.returncode

def read_file(path: str) -> str:
    return Path(path).read_text().rstrip()
