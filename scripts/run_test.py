#!/usr/bin/python3
import os
import sys
import time
import json
import shutil
from datetime import datetime

from fabric import Connection

from hist_to_json import *

def run_latester(ts: Connection, name: str, opts: str, dest_path: str):
	dst = dest_path+"/"
	ts.run("timeout 1m can-latester/_compiled/bin/latester -d can2 -d can3 -d can4 -n auto " + opts)
	for f in ["auto-hist.txt", "auto-stat.txt", "auto-msgs.txt"]:
		print("Downloading " + f)
		ts.get(f, local=dst+f)

	datefmt = datetime.now().strftime("%y%m%d-%H%M%S")
	shutil.copy2(dst+"auto-hist.txt", dst+f"run-{datefmt}-hist+{name}.txt")
	shutil.copy2(dst+"auto-stat.txt", dst+f"run-{datefmt}-stat+{name}.txt")
	#draw_hist

	hist_to_json(dst+f"run-{datefmt}-hist+{name}.txt")
	

def run_test(args: list[str], kernel_ver: str, test_branch, conf: dict):
	IP_TS = conf["IP_TS"] #"192.168.16.38"
	IP_GW = conf["IP_GW"] #"192.168.16.37"
	KEY_FILE = conf["KEY_FILE"]
	UGW_CMD = conf["UGW_CMD"] #"~/can-latester/_compiled/bin/ugw can3 can2"
	UGW_FD_CMD = conf["UGW_FD_CMD"]
	CGW_CMD = conf["CGW_CMD"] #"cangw -A -s can3 -d can2"
	CGW_FD_CMD = conf["CGW_FD_CMD"]
	STRESS = "stress" in args
	STRESS_CPUS = conf["STRESS_CPUS"] # 0..NUM_CPUS
	PING_FLOOD_TARGET = conf.get("PING_FLOOD_TARGET")
	can_fd = "fd" in args
	threaded_NAPI = "thrd" in args
	# RT priorities
	UGW_PRIO = conf["UGW_PRIO"] #80
	IRQ_PRIO = conf["IRQ_PRIO"] #90
	DEFAULT_PRIO = conf["DEFAULT_PRIO"] #50
	SET_RT = "rt" in args
	KERNEL_GW = "cangw" in args
	# Latester options
	opts = conf["OPTS_FLOOD"] if "flood" in args else conf["OPTS"]
	if can_fd: opts += " -f"
	# Where to copy results
	DEST_PATH = conf["DEST_PATH"]
	if test_branch is not None:
		DEST_PATH = os.path.join(DEST_PATH, test_branch)
	
	# Connect to boards
	ts = Connection(IP_TS, user="root", connect_kwargs={"key_filename": KEY_FILE})
	gw = Connection(IP_GW, user="root", connect_kwargs={"key_filename": KEY_FILE})

	#out = gw.run('uname -r')
	#name = out.splitlines()[0]

	print("-> Starting background processes...")
	# Start gateway
	cgw_cmd = CGW_FD_CMD if can_fd else CGW_CMD
	ugw_cmd = UGW_FD_CMD if can_fd else UGW_CMD 
	if KERNEL_GW:
		gw.run(cgw_cmd)
	else: # UGW
		gw_cmd = f"chrt -r {UGW_PRIO} {ugw_cmd}"
		gw_proc = gw.run(gw_cmd, asynchronous=True)
	irq_prio = IRQ_PRIO if SET_RT else DEFAULT_PRIO
	gw.run(f"~/can/set-irq-prio.sh {irq_prio}")
	# Set threaded NAPI if requested
	gw.run(f"~/can/set-can-threaded.sh {'1' if threaded_NAPI else '0'}")
	# Start CPU load
	if STRESS:
		stress_proc = gw.run(f"stress --cpu {STRESS_CPUS} --vm {STRESS_CPUS}", pty=True, asynchronous=True) # send_interrupt only works with pty
		io_proc = gw.run("echo 3 >/proc/sys/vm/drop_caches && find / -type f -exec cat '{}' ';' > /dev/null", pty=True, asynchronous=True)
		if PING_FLOOD_TARGET is not None:
			ping_proc = gw.run(f"ping -f {PING_FLOOD_TARGET}", asynchronous=True)
	# Let it settle
	time.sleep(1)

	# Run the test
	name = kernel_ver
	if "flood" in args:
		name += "+flood"
	else: name += "+oaat"
	if KERNEL_GW:
		name += "-kern"
	if SET_RT:
		name += "-prio"
	if can_fd:
		name += "-fd"
	if threaded_NAPI:
		name += "-thrd"
	if STRESS:
		name += "-load"
	print(f"-> Running test - id {name}")
	run_latester(ts, name, opts, DEST_PATH)

	# Stop CPU load and gateway
	print("-> Stopping background processes...")
	if STRESS:
		stress_proc.runner.send_interrupt(None)
		stress_proc.runner.kill()
		#gw.run("killall stress")
		io_proc.runner.send_interrupt(None)
		io_proc.runner.kill()
		#gw.run("killall find")
		if PING_FLOOD_TARGET is not None:
			ping_proc.runner.kill()
		time.sleep(2.0)
	if KERNEL_GW:
		gw.run("cangw -F")
	else:
		gw_proc.runner.kill()
		gw.run("killall ugw")
	time.sleep(0.5)

if __name__ == "__main__":
	confname = "/var/lib/latester/conf.json"
	conffile = open(confname)
	conf = json.load(conffile)
	conffile.close()

	run_test(sys.argv[1:-1], sys.argv[-1] if len(sys.argv) > 1 else "unk", None, conf)
