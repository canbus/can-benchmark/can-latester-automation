#!/bin/bash

#LINUX_SRC="/usr/src/linux-devel"
#cd $LINUX_SRC

git checkout master
git pull --ff-only || exit $?
git rev-parse --short --verify HEAD >/var/lib/latester/current-rev
# This will throw away previous autopatch branch
# It was created automatically (by this script) so nothing should be lost
git switch -C autopatch
patch_base.sh
exit $?

