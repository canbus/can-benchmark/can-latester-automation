#!/usr/bin/python3
import os
import sys

import argparse
from datetime import datetime
import json
import shutil

import build_web
from process_json_dir import process_json_dir, make_overview, generate_manifest
from utils import run_cmd

parser = argparse.ArgumentParser()
parser.add_argument("--make-archive", type=int, metavar="YEAR", help="The year for which to generate web data bundle")
parser.add_argument("--keep-months", type=int, metavar="N", required=False, help="Keep past N months in result dir, move older to archive")
args = parser.parse_args()

year = args.make_archive
if year is not None and (year < 2022 or year > 2099):
    # File names only contain last 2 digits
    print("Year is not in range 2022-2099", file=sys.stderr)
    exit(1)

conf_file = os.environ.get("LATESTER_CONF", "/var/lib/latester/conf.json")
with open(conf_file) as cfile:
    conf = json.load(cfile)

RESULTS_DIR = conf["DEST_PATH"]
TEST_BRANCHES = list(conf["KERNEL_VARIANTS"].keys())
ARCHIVE_DIR = conf["RESULTS_ARCHIVE_DIR"]
WEB_DIR = conf["WEB_DIR"]
WEB_ARCHIVE_DIR = conf["WEB_ARCHIVE_DIR"]
WEB_EXPORT_DIR = conf["WEB_EXPORT_DIR"]
OVERVIEW_CONFIGS = conf["OVERVIEW_CONFIGS"]

web_results = os.path.join(WEB_DIR, "results")
if year is not None:
    web_archive = os.path.join(WEB_ARCHIVE_DIR, str(year), "results")
    if not os.path.exists(web_archive):
        os.makedirs(web_archive)

for b in TEST_BRANCHES:
    test_dir = os.path.join(RESULTS_DIR, b)
    if year is not None:
        archive_test_dir = os.path.join(ARCHIVE_DIR, str(year), b)
        process_json_dir([test_dir, archive_test_dir], web_archive, year)
    if args.keep_months is not None:
        n_moved = 0
        for f in os.listdir(test_dir):
            if f.startswith(f"run-"):
                # if older than keep-months
                date = datetime.strptime(f[4:17], "%y%m%d-%H%M%S")
                now = datetime.now()
                if (now - date).days > 31*args.keep_months:
                    # Move to archive of appropriate name
                    archive_dest = os.path.join(ARCHIVE_DIR, str(date.year), b)
                    os.makedirs(archive_dest, exist_ok=True)
                    shutil.move(os.path.join(test_dir, f), archive_dest)
                    n_moved += 1
        print(f"Moved {n_moved} files to archive")
        process_json_dir(test_dir, web_results)

if year is not None:
    make_overview(web_archive, OVERVIEW_CONFIGS, os.path.join(web_archive, "overview.json"))
    generate_manifest(web_archive)
    cur_web_archive = os.path.join(WEB_ARCHIVE_DIR, str(year))
    build_web.build_all(cur_web_archive, archive=True)
    # Create tar for export
    run_cmd(["tar", "-czhf", f"{WEB_EXPORT_DIR}/can-latester-results-{year}.tar.gz", "-C", WEB_ARCHIVE_DIR, str(year)], shell=False)
    
if args.keep_months is not None:
    make_overview(web_results, OVERVIEW_CONFIGS, os.path.join(web_results, "overview.json"))
    generate_manifest(web_results)
