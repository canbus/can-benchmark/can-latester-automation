#!/usr/bin/python3
import sys
import matplotlib.pyplot as plt

histp = sys.argv[1]
histf = open(histp)
histbins = []
histcum = []
for l in histf:
	ls = l.strip()
	if len(ls) < 1: continue
	la = ls.split()
	histbins.append(float(la[0]))
	histcum.append(int(la[1]))

if "--bar" in sys.argv:
	hist = [0 for _ in range(len(histbins))]
	hist[-1] = histcum[-1]
	for i in range(len(histbins)-2,-1,-1):
		hist[i] = histcum[i]-histcum[i+1]

	plt.bar(histbins, hist, width=0.001)
else:
	plt.plot(histbins, histcum)
plt.grid()
plt.xlabel("latency (ms)")
plt.ylabel("messages (-)")
plt.yscale('log')
#plt.ylim(1, 100000)

# Save plot to file if last two args are "--save <filename>"
if len(sys.argv) > 3 and sys.argv[-2] == "--save":
	plt.savefig(sys.argv[-1])
else:
	plt.show()

