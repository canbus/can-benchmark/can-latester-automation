function updateparam(param, value) {
    let params = new URLSearchParams(location.search);
    if (value == "")
        params.delete(param)
    else
        params.set(param, value);
    location.search = params.toString();
}
