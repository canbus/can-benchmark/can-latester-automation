var jobjs = {};
var selectedConfigs = [];
var selectedHists = [];
var listenersAdded = false;

function histLoaded(conf, jstr) {
    jobjs[conf] = JSON.parse(jstr);
    drawTraces();
}

function drawTraces() {
    let data = [];
    for (let i = 0; i < selectedConfigs.length; i++) {
        let jobj = jobjs[selectedConfigs[i]];
        let xdates = [];
        for (let j = 0; j < jobj.labels.length; j++) {
            let d = jobj.labels[j].split("-")[1];
            xdates.push(new Date("20"+d.substring(0,2)+"-"+d.substring(2,4)+"-"+d.substring(4)));
        }
        let min = [];
        let max = []
        let avg = [];
        for (let j = 0; j < jobj.stats.length; j++) {
            min.push(jobj.stats[j].min);
            avg.push(jobj.stats[j].average);
            max.push(jobj.stats[j].max);
        }
        let ydata;
        if (selectSeries.selectedIndex == 0)
            ydata = max;
        else if (selectSeries.selectedIndex == 1)
            ydata = min;
        else ydata = avg;
        data.push({
            x: xdates,
            y: ydata,
            type: "scatter",
            name: selectedConfigs[i]
        });
    }
    let layout = {
        margin: { t: 10, b: 10 },
        xaxis: {
            rangeslider: {}
        },
        yaxis: {
            title: "Latency (ms)",
            type: "log"
        }
    };
    Plotly.react("plot", data, layout);
    var hoverInfo = document.getElementById('hoverinfo');
    if (!listenersAdded) {
        listenersAdded = true;
        plot.on('plotly_hover', function(data){
            let jobj = jobjs[selectedConfigs[data.points[0].curveNumber]];
            hoverInfo.innerHTML = jobj.labels[data.points[0].pointIndex];
        });
        plot.on('plotly_unhover', function(data){
            hoverInfo.innerHTML = "<br>";
        });
        plot.on('plotly_click', function(data){
            let series = selectedConfigs[data.points[0].curveNumber]
            let run = data.points[0].pointIndex;
            selectHist(series, run);
        });
    }
}

function drawHists() {
    let plot2data = [];
    for (let hist of selectedHists) {
        let histId = hist.split("/");
        let jobj = jobjs[histId[0]];
        var xdata = jobj.x;
        var ydata = jobj.y[Number(histId[1])];
        for (var i = ydata.length-1; i > 0; i--) {
            if (ydata[i] != 0) {
                ydata = ydata.slice(0, i+2);
                xdata = xdata.slice(0, i+2);
                break;
            }
        }
        let cumul = ydata.slice(); //copy
        for (var i = cumul.length-2; i >= 0; i--) {
            cumul[i] += cumul[i+1];
        }
        plot2data.push({
            x: xdata,
            y: cumul,
            name: hist,
        });
    }
    plot2layout = { 
        margin: { t: 10 },
        xaxis: {
            title: "latency (ms)",
        },
        yaxis: {
            type: 'log',
            autorange: true,
            fixedrange: true
        },
    };
    Plotly.react("plot2", plot2data, plot2layout);
}

function toggledropdown() {
    document.getElementById("dropdown-add-test").classList.toggle("showhidden");
}

function dismissDropdowns(evt) {
    if (!evt.target.matches('.dropdownbtn')) {
        let dropdowns = document.getElementsByClassName("dropdown-content");
        for (let d of dropdowns)
            d.classList.remove("showhidden");
    }
}

function loadData(confName, archiveYear) {
    let xhr = new XMLHttpRequest();
    let archiveDir = ""
    if (archiveYear != null)
        archiveDir = "../can-latester-archive/" + archiveYear + "/"
    let url = archiveDir + "results/" + confName + ".json";
    xhr.open("GET", url);
    xhr.addEventListener("load", () => histLoaded(confName, xhr.responseText));
    xhr.send();
}

function selectHist(series, run) {
    let id = series + "/" + run;
    if (!selectedHists.includes(id)) {
        selectedHists.push(id);
        let p = document.createElement("p");
        let span = document.createElement("span");
        span.textContent = "❌";
        span.style.cursor = "pointer";
        span.onclick = () => {
            selectedHists.splice(selectedHists.indexOf(id), 1);
            p.remove();
            drawHists();
        }
        p.className = "rm-hist";
        p.appendChild(span);
        p.append(" " + id + ": " + jobjs[series].labels[run]);
        document.getElementById("hist-box").appendChild(p);
        drawHists();
    }
}

function clearHists() {
    let histBtns = document.getElementsByClassName("rm-hist");
    while (histBtns[0]) {
        histBtns[0].remove()
    }
    selectedHists = [];
    drawHists();
}

function selectConfig(config, skipDraw=false) {
    if (!selectedConfigs.includes(config)) {
        selectedConfigs.push(config);
        let params = new URLSearchParams(location.search);

        // Create button for the config, clicking it goes to inspect page
        let btn = document.createElement("div");
        btn.className = "button";
        let title = document.createElement("a");
        title.innerText = config;
        let confParams = config.split("-");
        let inspectHref = "inspect.html?kernel=" + confParams[0];
        if (params.has("archive"))
            inspectHref += "&archive=" + params.get("archive");
        for (let i = 1; i < confParams.length; i++) {
            if (confParams[i] == "oaat") continue;
            inspectHref += "&"+confParams[i]+"=1";
        }
        if (!confParams.includes("kern")) // kern is on by default
            inspectHref += "&kern=0"
        title.href = inspectHref;
        // Create X (cross) half of the button
        let xbtn = document.createElement("div");
        xbtn.className = "sub-button";
        xbtn.innerText = "X";
        xbtn.onclick = function () {
            selectedConfigs.splice(selectedConfigs.indexOf(config), 1);
            params.set("configs", selectedConfigs.join(","));
            history.replaceState(null, "", location.pathname + "?" + params.toString());
            btn.remove();
            drawTraces();
        };
        btn.appendChild(title);
        btn.appendChild(xbtn);
        document.getElementById("main-box").appendChild(btn);

        if (!Object.keys(jobjs).includes(config))
            loadData(config, params.get("archive"));
        else if (!skipDraw) {
            drawTraces();
        }
        if (!skipDraw) {
            params.set("configs", selectedConfigs.join(","));
            history.replaceState(null, "", location.pathname + "?" + params.toString());
        }
    }
}

var selectSeries;
window.onload = async function() {
    let params = new URLSearchParams(location.search);

    let manifestURL = "results/manifest.json";
    if (params.has("archive"))
        manifestURL = "../can-latester-archive/" + params.get("archive") + "/" + manifestURL;
    const manifestResp = await fetch(manifestURL);
    if (!manifestResp.ok) {
        alert("Could not load manifest: " + manifestURL);
        return;
    }
    const manifest = await manifestResp.json();
    const dropdown = document.getElementById("dropdown-add-test");
    for (const child of dropdown.children) {
        const testConf = child.innerText.split('-'); //kernel-opt1-opt2...
        const optsAvailable = manifest.kernelOpts[testConf[0]]
        for (const opt of testConf.slice(1)) {
            if (!optsAvailable.includes(opt) && opt != "oaat") {
                child.classList.add("hidden");
                break;
            }
        }
    }

    selectSeries = document.getElementById("select-series");
    selectSeries.addEventListener('change', () => {
        drawTraces();
        console.log("Selected " + selectSeries.selectedIndex);
    });
    if (params.has("configs")) {
        let configs = params.get("configs").split(",");
        for (const c of configs) {
            selectConfig(c, true);
        }
    } else selectConfig("master-oaat");
    window.addEventListener('click', dismissDropdowns);
}

