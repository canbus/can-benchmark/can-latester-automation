var surfaceView = false;
var heatmap = false;
var jobj = null;
var listenersAdded = false;

function histLoaded(_jobj) {
    jobj = _jobj;

    drawMainGraph();
    drawHist2(jobj, jobj.y.length-1);
    var min = jobj.stats[0].min;
    var max = jobj.stats[0].max;
    var avg = jobj.stats[0].average;
    for (var i = 1; i < jobj.stats.length; i++) {
        if (jobj.stats[i].min < min)
            min = jobj.stats[i].min;
        if (jobj.stats[i].max > max)
            max = jobj.stats[i].max;
        avg += jobj.stats[i].average;
    }
    avg /= jobj.stats.length;
    l_best.innerHTML = "Best: " + min + " ms";
    l_worst.innerHTML = "Worst: " + max + " ms";
    l_avg.innerHTML = "Average: " +  avg.toFixed(3) + " ms";

    if (listenersAdded)
        return;
    var plot = document.getElementById('plot');
    var hoverInfo = document.getElementById('hoverinfo');
    plot.on('plotly_hover', function(data){
        if (surfaceView) {
            var p = data.points[0];
            hoverInfo.innerHTML = jobj.labels[p.y];
        } else hoverInfo.innerHTML = jobj.labels[data.points[0].pointIndex];
    });
    plot.on('plotly_unhover', function(data){
        hoverInfo.innerHTML = "<br>";
    });
    plot.on('plotly_click', function(data){
        if (surfaceView) {
            var p = data.points[0];
            //loadHist(jobj.labels[p.y]);
            drawHist2(jobj, p.y);
        } else drawHist2(jobj, data.points[0].pointIndex);
    });
    listenersAdded = true;
}

function drawArea(jobj) {
    var data = [];
    var min = [];
    var max = [];
    var mmax = 0;
    var avg = [];
    for (var i = 0; i < jobj.stats.length; i++) {
        min.push(jobj.stats[i].min)
        avg.push(jobj.stats[i].average)
        max.push(jobj.stats[i].max)
        if (jobj.stats[i].max > mmax)
            mmax = jobj.stats[i].max;
    }
    xdates=[];
    for (var i = 0; i < jobj.labels.length; i++) {
        var d = jobj.labels[i].split("-")[1];
        xdates.push(new Date("20"+d.substring(0,2)+"-"+d.substring(2,4)+"-"+d.substring(4)));
    }
    data.push({
        x: xdates,
        y: min,
        type: "scatter",
        mode: "lines",
        line: {width: 0}, 
        name: "min"
    });
    data.push({
        x: xdates,
        y: avg,
        fill: "tonexty",
        fillcolor: "rgba(0, 0, 100, 0.3)",
        type: "lines",
        name: "average",
        //hovertext: jobj.labels
    });
    data.push({
        x: xdates,
        y: max,
        fill: "tonexty",
        fillcolor: "rgba(0, 0, 100, 0.3)",
        line: {width: 0}, 
        marker: {color: "444"}, 
        type: "scatter",
        mode: "lines",
        name: "max"
    });
    var layout = {
        margin: { t: 0 },
        showlegend: false,
        xaxis: {
            title: "Date",
            rangeslider: {}
        },
        yaxis: {
            title: "Latency (ms)",
            fixedrange: true
        },
        hovermode: "x"
    };
    if (mmax > 5)
        layout.yaxis.range = [0, 5];
    Plotly.react( "plot", data, layout, {displayModeBar:false, modeBarButtonsToRemove: ['select2d', 'lasso2d', 'hoverClosestCartesian', 'hoverCompareCartesian', 'toggleSpikelines']});
}

function makeLogSafe(arr) {
    for (var i = 0; i < arr.length; i++) {
        var series = arr[i].slice();
        for (var j = 0; j < series.length; j++) {
            if (series[j] < 1)
                series[j] = 0.5;
        }
        arr[i] = series;
    }
    return arr;
}

function drawSurface(jobj) {
    var z_data = heatmap ? jobj.y : makeLogSafe(jobj.y.slice());
    var layout = { 
        margin: { t: 20, b: 0 },
        xaxis: { title: "latency (ms)" },
        yaxis: { title: "run index" },
        scene: {
            aspectmode: "manual",
            aspectratio: {x: 1, y: 2, z:1 },
            xaxis: {
                showspikes: false,
                title: "latency (ms)",
                //autotick: false,
                //dtick: 0.02,
                //tickmode: "auto",
                nticks: 8
            },
            yaxis: {
                showspikes: false,
                title: "run index",
                type: "category"            
            },
            zaxis: {
                showspikes: false,
                title: "frames",
                type: 'log',
                autorange: true,
            },
            camera: {eye: {x: 1.8, y: -2.0, z: 0.5}}
        }
    };
    var data = [{
        x: jobj.x,
        z: z_data,
        contours: {
            x: {
                highlight: false,
                project: {x: false, y:false, z:false}
            },
            y: {
                highlight: true,
                project: {x: false, y:false, z:false}
            },
            z: {
                highlight: false,
            }
        },
        showscale: heatmap,
        //hoverinfo: "none"
        colorscale: [
            ['0.0', '#FFE7C2'],
            ['0.0005', '#FFA569'],
            ['0.03125', '#FF884D'],
            ['0.0625', '#FF6A33'],
            ['0.125', '#EB572F'],
            ['0.25', '#D7462B'],
            ['0.5', '#C33727'],
            ['1.0', '#9C1F21']
        ],
        type: heatmap ? "heatmap" : "surface",
    }];
    if (heatmap) {
        layout.margin.b = 40;
    }
    Plotly.react( "plot", data, layout );
}


var plot2data = null;
var plot2layout = null;
var plot2path = "";
function drawHist2(jobj, idx) {
    //var jstr = this.responseText;
    //var jobj = JSON.parse(jstr);
    var xdata = jobj.x;
    var ydata = jobj.y[idx];
    for (var i = ydata.length-1; i > 0; i--) {
        if (ydata[i] != 0) {
            ydata = ydata.slice(0, i+2);
            xdata = xdata.slice(0, i+2);
            break;
        }
    }
    cummul = ydata.slice(); //copy
    for (var i = cummul.length-2; i >= 0; i--) {
        cummul[i] += cummul[i+1];
    }

    if (plot2data == null) {
        plot2data = [{
            x: xdata,
            y: ydata,
            ndata: ydata,
            cdata: cummul,
            type: "bar",
            cummulative: {enabled: false}
        }];
        plot2layout = { 
            margin: { t: 0 },
            xaxis: {
                title: "latency (ms)",
                //rangeslider: {}
            },
            yaxis: {
                type: 'log',
                autorange: true,
                fixedrange: true
            },
            title: plot2path
        };
    } else {
        plot2data[0].x = xdata;
        plot2data[0].y = plot2data[0].cummulative.enabled ? cummul : ydata;
        plot2data[0].ndata = ydata;
        plot2data[0].cdata = cummul;
        plot2layout.title = plot2path;
    }
    Plotly.react("plot2", plot2data, plot2layout);

    pCurrentHistId.innerText = jobj.labels[idx];
    s_5th.innerHTML = "5th percentile: " + jobj.stats[idx].perc5 + " ms";
    s_95th.innerHTML = "95th percentile: " + jobj.stats[idx].perc95 + " ms";
    s_best.innerHTML = "Best: " + jobj.stats[idx].min + " ms";
    s_worst.innerHTML = "Worst: " + jobj.stats[idx].max + " ms";
    s_med.innerHTML = "Median: " + jobj.stats[idx].med + " ms";
    var lost = jobj.stats[idx].lost;
    s_lost.innerHTML = "Lost: " + lost + " (" + (lost/(lost+cummul[0])*100).toFixed(2) + " %)";
}

function loadHist(path) {
    plot2path = path;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", path);
    xhr.addEventListener("load", hist2Loaded);
    xhr.send();
}

function togglecumulative() {
    var data = plot2data[0];
    data.cummulative.enabled = !data.cummulative.enabled;
    if (data.cummulative.enabled) {
        data.y = data.cdata;
        data.type = "scatter";
    } else {
        data.y = data.ndata;
        data.type = "bar";
    }
    console.log("cummulative: " + plot2data[0].cummulative.enabled);
    Plotly.redraw("plot2");
}

function togglesurface() {
    surfaceView = true;
    heatmap = false;
    drawMainGraph();
}

function toggleheatmap() {
    heatmap = surfaceView = true;
    drawMainGraph();
}

function togglelineplot() {
    heatmap = surfaceView = false;
    drawMainGraph();
}

function drawMainGraph() {
    if (surfaceView || heatmap)
        drawSurface(jobj);
    else
        drawArea(jobj);
}

function togglecollapsible(obj) {
    var content = obj.nextElementSibling;
    obj.classList.toggle("collapsed");
    if (content.style.maxHeight){
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    } 
}

function toggleparam(key) {
    var params = new URLSearchParams(location.search);
    if (opts[key])
        params.set(key, "0");
    else
        params.set(key, "1");
    history.replaceState(null, "", location.pathname + "?" + params.toString());
    applyParams(params);
}

function switchkernel(kern) {
    var params = new URLSearchParams(location.search);
    params.set("kernel", kern);
    history.replaceState(null, "", location.pathname + "?" + params.toString());
    applyParams(params);
}

var s_lost, s_best, s_worst, s_med, s_95th, s_5th;
var l_best, l_worst, l_avg;
var pCurrentHistId;

var kernel = "master";
var opts = {
    "flood": false,
    "kern": true,
    "prio": false,
    "fd": false,
    "load": false,
    "thrd": false
}
var optsForKernel = {};
window.onload = async function() {
    s_lost = document.getElementById("s_lost");
    s_best = document.getElementById("s_best");
    s_worst = document.getElementById("s_worst");
    s_med = document.getElementById("s_med");
    s_95th = document.getElementById("s_95th");
    s_5th = document.getElementById("s_5th");
    l_best = document.getElementById("l_best");
    l_worst = document.getElementById("l_worst");
    l_avg = document.getElementById("l_avg");
    pCurrentHistId = document.getElementById("currenthistid");
    var params = new URLSearchParams(location.search);

    let manifestURL = "results/manifest.json";
    if (params.has("archive"))
        manifestURL = "../can-latester-archive/" + params.get("archive") + "/" + manifestURL;
    const manifestResp = await fetch(manifestURL);
    if (!manifestResp.ok) {
        alert("Could not load manifest: " + manifestURL);
        return;
    }
    optsForKernel = (await manifestResp.json()).kernelOpts;
    // Disable unavailable kernel buttons (statically generated)
    let availableKernels = Object.keys(optsForKernel);
    for (const kern of kernels) {
        if (!availableKernels.includes(kern)) {
            let kernBtn = document.getElementById("kern"+kern);
            kernBtn.classList.add("btndisabled");
            kernBtn.onclick = null;
        }
    }

    applyParams(params);
}

function applyParams(params) {    
    if (params.has("kernel"))
        kernel = params.get("kernel");
    var availableOpts = optsForKernel[kernel];
    for (const opt of Object.keys(opts)) {
        if (params.has(opt))
            opts[opt] = params.get(opt) === "1";
        if (!availableOpts.includes(opt))
            opts[opt] = false;
    }

    // Toggle kernel radio buttons
    for (const kern of kernels)
        document.getElementById("kern"+kern).classList.toggle("btnactive", kernel == kern);

    // Toggle option checkbox buttons
    for (const opt of Object.keys(opts)) {
        var optBtn = document.getElementById("tgl"+opt);
        if (availableOpts.includes(opt)) {
            optBtn.classList.toggle("btnactive", opts[opt]);
            optBtn.classList.toggle("btndisabled", false);
        } else {
            optBtn.classList.toggle("btnactive", false);
            optBtn.classList.toggle("btndisabled", true);
        }
    }

    // Get results json and display data
    let year = ""
    if (params.has("archive"))
        year = "../can-latester-archive/" + params.get("archive") + "/"
    let path = year + "results/" + kernel;
    if (opts.flood) path += "-flood";
    else path += "-oaat";
    for (const opt of Object.keys(opts)) {
        if (opt == "flood") continue;
        if (opts[opt]) path += "-" + opt;
    }
    path += ".json";
    
    fetch(path).then((res) => res.json()).then((j => histLoaded(j)));
}
