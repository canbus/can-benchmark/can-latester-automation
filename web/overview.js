var jobj = null;
var cummulativeHist = false;

function histLoaded(_j) {
    jobj = _j;

    drawTraces(jobj);
    drawHists(jobj);
}

function drawTraces(jobj) {
    var data = [];
    for (var i = 0; i < jobj.xs.length; i++) {
        var xdates = []
        for (var j = 0; j < jobj.xs[i].length; j++)
            xdates.push(new Date(jobj.xs[i][j]*1000))
        data.push({
            x: xdates,
            y: jobj.ys[i],
            type: "scatter",
            name: jobj.labels[i]
        });
    }
    var layout = {
        margin: { t: 10, b: 10 },
        xaxis: {
            rangeslider: {}
        },
        yaxis: {
            title: "Worst latency (ms)",
            type: "log"
        }
    };
    Plotly.react("plot", data, layout);
}

function drawHists(jobj) {
    for (var i = 0; i < jobj.labels.length; i++)
        drawHist(jobj, i);
}

function drawHist(jobj, idx) {
    var plot2data = [{
        x: jobj.last_hists[idx].x,
        y: jobj.last_hists[idx].y,
        /*ndata: jobj.y[idx],
        cdata: cummul,*/
        type: "bar",
    }];
    if (cummulativeHist) {
        var cummul = jobj.last_hists[idx].y.slice(); //copy
        for (var i = cummul.length-2; i >= 0; i--) {
            cummul[i] += cummul[i+1];
        }
        plot2data[0].y = cummul;
        plot2data[0].type = "scatter";
    }
    var plot2layout = { 
        margin: { t: 0, l: 30, r: 10 },
        xaxis: {
            title: "latency (ms)",
            //rangeslider: {}
        },
        yaxis: {
            type: 'log',
            autorange: true,
            fixedrange: true
        },
        title: jobj.last_hists[idx].label.split("+")[-1]
    };
    Plotly.react("plot"+idx, plot2data, plot2layout);
    document.getElementById("plottitle"+idx).innerText = jobj.labels[idx]; 
}

function togglecumulative() {
    cummulativeHist = !cummulativeHist;
    drawHists(jobj);
}

function togglecollapsible(obj) {
    var content = obj.nextElementSibling;
    obj.classList.toggle("collapsed");
    if (content.style.maxHeight){
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    } 
}

window.onload = async function() {
    let params = new URLSearchParams(location.search);
    let year = ""
    if (params.has("archive"))
        year = "../can-latester-archive/" + params.get("archive") + "/"
    let overviewUrl = year + "results/" + "overview.json";
    fetch(overviewUrl).then((res) => res.json()).then((j => histLoaded(j)));

    const res = await fetch("results/retcode");
    const txt = await res.text()
    const code = txt.trimEnd();
    const lastMod = res.headers.get("Last-Modified");
    let statusText = "";
    if (code !== "0") {
        statusText = "Last execution of daily tests at " + lastMod + " ended unsuccessfully (return code " + code + ")";
    } else {
        statusText = "Last execution of daily tests at " + lastMod + " finished successfully";
    }
    document.getElementById("last-exec-status").innerText = statusText;
}
