#!/bin/sh

for ifc in can2 can3 can4 can5 ; do
  /bin/ip link set $ifc down
  /bin/ip link set $ifc type can bitrate 500000 dbitrate 2000000 fd on
  /bin/ip link set $ifc type can restart-ms 100
  /bin/ip link set $ifc up
done

