#!/bin/bash

for EIF in /sys/class/net/e* ; do
    if [ "$(<$EIF/operstate)" == "up" ] ; then
        if [ "$(<$EIF/type)" == "1" ] ; then
            MAC="$(<$EIF/address)"
            if [ -n "$MAC" ] ; then
                break
            fi
        fi
    fi
done

test "$MAC" = "00:0a:35:00:22:04"; IS_TS=$?

if dpkg --compare-versions $(uname -r) lt "5.19" ; then
    echo "-> Loading kernel module... (Linux <5.19)"
    insmod /opt/zynq/ctucanfd/ctucanfd.ko
    insmod /opt/zynq/ctucanfd/ctucanfd_platform.ko
fi

echo "-> Updating FPGA"
cd /opt/zynq/upbit
/opt/zynq/upbit/upbit

# some wait to allow CTU CAN FD drivers to initialize
sleep 2

echo "-> Setting up can..."
setup-can.sh
# Increase txqueuelen on both devices' tx interface to prevent ENOBUF
ip link set can2 txqueuelen 500

if [ "$IS_TS" = "0" ]; then
    CROSSBAR="0x03040150"
else
    CROSSBAR="0x03040111"
fi
echo "-> Setting up crossbar: $CROSSBAR"
rdwrmem -b4 -s 0x43C10000 -l4 -F $CROSSBAR

if [ "$IS_TS" = "0" ]; then
    echo "-> UIO crossbar driver"
    modprobe uio_pdrv_genirq of_id=can-crossbar
    rmmod uio_pdrv_genirq
    modprobe uio_pdrv_genirq of_id=can-crossbar

    echo "-> Rise CAN irq priorities"
    PIDS=$(ps -e | grep -E irq/[0-9]+-can[3-4] | tr -s ' ' | cut -d ' ' -f2)
    TXPID=$(ps -e | grep -E irq/[0-9]+-can2 | tr -s ' ' | cut -d ' ' -f2)
    chrt -f --pid 80 $TXPID
    for pid in $PIDS ; do
        chrt -f --pid 85 $pid
    done
fi

