#!/bin/bash

NAPI_PRIO=80

if (( $# != 1 )); then
  echo "Need 1 argument (value 0 or 1)" >&2
  exit 1
fi

for ifc in can2 can3 can4 can5 ; do
  echo $1 > /sys/class/net/$ifc/threaded
done

if [ $1 -eq 1 ] ; then
  PIDS=$(ps -e | grep -E napi/can[0-9]+-[0-9] | tr -s ' ' | cut -d ' ' -f2)
  for pid in $PIDS ; do
    echo "Setting RT priority $NAPI_PRIO for $pid"
    chrt -f --pid $NAPI_PRIO $pid
  done
fi
