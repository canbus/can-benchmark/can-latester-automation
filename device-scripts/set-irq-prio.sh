#!/bin/bash
if [ ! -z $1 ] ; then
PRIO=$1
else
PRIO=90
fi

PIDS=$(ps -e | grep -E irq/[0-9]+-can[0-9] | tr -s ' ' | cut -d ' ' -f2)
for pid in $PIDS ; do
	echo "Setting RT priority $PRIO for $pid"
	chrt -f --pid $PRIO $pid
done
